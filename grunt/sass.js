module.exports = {
	prod: {
		files: {
			'style.css': 'assets/scss/style.scss',
		},
	},
	dist: {
		options: {
			sourcemap: 'none',
		},
		files: {
			'style.css': 'assets/scss/style.scss'
		}
	}
};
