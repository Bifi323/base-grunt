module.exports = {
  task_name: {
    options: {
      // Task-specific options go here.
    }
  },
  watch: {
    options: {
      title: 'Task Complete', // optional
      message: 'Message text', //required
    }
  }
};
